package Controllers

import (
	DB "budgetBackendGo/Database"
	Entity "budgetBackendGo/Entities"
	"budgetBackendGo/Helpers/UserHelpers"
	Services "budgetBackendGo/Services"
	Mappers "budgetBackendGo/Types/Mappers"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ListCategory(c *gin.Context) {
	// UserHelpers.ValidateToken(c)
	db := DB.GormDb
	var Category []Entity.CategoryEntity
	err := db.Find(&Category)
	if err != nil {
		c.JSON(http.StatusBadRequest, "")
	}
	c.JSON(http.StatusOK, Category)
}

func ListCategoryExpense(c *gin.Context) []Entity.CategoryEntity {
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))
	Category := Services.ListCategoryExpense(c, *userId)
	c.JSON(http.StatusOK, Mappers.CategoryEntityListToDto(Category))
	return Category
}

func ListCategoryIncome(c *gin.Context) []Entity.CategoryEntity {
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))
	Category := Services.ListCategoryIncome(c, *userId)
	c.JSON(http.StatusOK, Mappers.CategoryEntityListToDto(Category))
	return Category
}

func ReadCategory(c *gin.Context) {
	// UserHelpers.ValidateToken(c)
	id := c.Param("id")
	db := DB.GormDb
	var Category Entity.CategoryEntity
	err := db.Debug().First(&Category, id).Error
	if err != nil {
		c.JSON(http.StatusNotFound, "Record Does Not Exist")
		panic(err)
	}
	CategoryDTO := Mappers.CategoryEntityToDto(Category)
	c.JSON(http.StatusOK, CategoryDTO)
}

func CreateCategory(c *gin.Context) {
	// UserHelpers.ValidateToken(c)
	db := DB.GormDb
	var Category Entity.CategoryEntity
	err := c.ShouldBindJSON(&Category)
	if err != nil {
		c.JSON(http.StatusBadRequest, "")
		panic(err)
	}
	createErr := db.Create(&Category)
	if createErr != nil {
		c.JSON(http.StatusBadRequest, "")
	}
	var CreatedCategory Entity.CategoryEntity
	firstErr := db.First(&CreatedCategory, Category.Id)
	if firstErr != nil {
		c.JSON(http.StatusBadRequest, "")
	}
	c.JSON(http.StatusOK, CreatedCategory)
}

func UpdateCategory(c *gin.Context) {
	// UserHelpers.ValidateToken(c)
	db := DB.GormDb
	var Category Entity.CategoryEntity
	err := c.ShouldBindJSON(&Category)
	if err != nil {
		c.JSON(http.StatusBadRequest, "")
		panic(err)
	}
	saveErr := db.Save(&Category)
	if saveErr != nil {
		c.JSON(http.StatusBadRequest, "")
	}
	c.JSON(http.StatusOK, Category)
}

func DeleteCategory(c *gin.Context) {
	// UserHelpers.ValidateToken(c)
	id := c.Param("id")
	db := DB.GormDb
	deleteErr := db.Delete(&Entity.CategoryEntity{}, id)
	if deleteErr != nil {
		c.JSON(http.StatusBadRequest, "")
	}
	c.JSON(http.StatusOK, "")
}
