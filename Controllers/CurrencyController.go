package Controllers

import (
	DB "budgetBackendGo/Database"
	SqlMappers "budgetBackendGo/Database/Mappers"
	SqlRequests "budgetBackendGo/Database/SQL"
	"budgetBackendGo/Helpers/UserHelpers"
	DTO "budgetBackendGo/Types/DTO"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func ListCurrencies(c *gin.Context) {
	UserHelpers.ValidateToken(c)
	db := DB.DbConnection
	response, err := db.Query(SqlRequests.GetCurrencies())
	if err != nil {
		log.Default().Println(err)
		panic(err)
	}
	c.JSON(http.StatusOK, SqlMappers.CurrencyRowsMapper(response))
}

func CreateCurrency(c *gin.Context) {
	UserHelpers.ValidateToken(c)
	var currency DTO.CurrencyDto
	db := DB.DbConnection
	err := c.ShouldBindJSON(&currency)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid Request")
		return
	}
	response, err := db.Query(SqlRequests.CreateCurrency(currency))
	if err != nil {
		log.Default().Fatalln(err)
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			c.JSON(http.StatusConflict, "Currency Already Exists")
		}
	}
	c.JSON(http.StatusOK, SqlMappers.CurrencyRowMapper(response))
}

func UpdateCurrency(c *gin.Context) {
	UserHelpers.ValidateToken(c)
	var currency DTO.CurrencyDto
	db := DB.DbConnection
	err := c.ShouldBindJSON(&currency)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid Request")
		return
	}
	fmt.Println(SqlRequests.UpdateCurrency(currency))
	response, err := db.Query(SqlRequests.UpdateCurrency(currency))
	if err != nil {
		log.Default().Fatalln(err)
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			c.JSON(http.StatusConflict, "Currency Already Exists")
		}
	}
	c.JSON(http.StatusOK, SqlMappers.CurrencyRowMapper(response))
}

func DeleteCurrency(c *gin.Context) {
	UserHelpers.ValidateToken(c)
	var currency DTO.CurrencyDto
	db := DB.DbConnection
	err := c.ShouldBindJSON(&currency)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid Request")
		return
	}
	fmt.Println(SqlRequests.DeleteCurrency(currency.ID))
	response, err := db.Query(SqlRequests.DeleteCurrency(currency.ID))
	if err != nil {
		log.Default().Fatalln(err)
		panic(err)
	}
	fmt.Println(response)
	c.JSON(http.StatusOK, "")
}
