package Controllers

import (
	"budgetBackendGo/Helpers/UserHelpers"
	"budgetBackendGo/Services"
	DTO "budgetBackendGo/Types/DTO"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func GetTimedBudget(c *gin.Context) {
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))
	var dateRange DTO.DateRangeDto
	err := c.ShouldBindJSON(&dateRange)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid Request")
		return
	}
	budget := Services.GetTimedBudget(c, dateRange, *userId)
	var dateRangeJava DTO.DateRangeDtoJava = DTO.DateRangeDtoJava{}
	dateRangeJava.StartDateTime = dateRange.StartDateTime.Format(time.RFC3339Nano)
	dateRangeJava.EndDateTime = dateRange.EndDateTime.Format(time.RFC3339Nano)
	jsonData, err := json.Marshal(dateRangeJava)
	if err != nil {
		c.JSON(400, "Bad Request")
		panic(err)
	}
	req, err := http.NewRequest("POST", "https://budgebackendpether.herokuapp.com/budget/", bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	req.Header.Add("Authorization", UserHelpers.GetToken(c))
	if err != nil {
		c.IndentedJSON(400, "Bad Request")
		panic(err)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		c.IndentedJSON(400, "Bad Request")
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		c.IndentedJSON(400, "Bad Request")
		panic(err)
	}
	var budgetDto = new(DTO.BudgetDto)
	json.Unmarshal(body, &budgetDto)
	c.JSON(200, budget)
}
