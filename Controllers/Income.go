package Controllers

import (
	DB "budgetBackendGo/Database"
	Entity "budgetBackendGo/Entities"
	"budgetBackendGo/Helpers/UserHelpers"
	"budgetBackendGo/Services"
	DTO "budgetBackendGo/Types/DTO"
	"budgetBackendGo/Types/Mappers"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ListIncome(c *gin.Context) {
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))
	IncomeEntity := Services.ListIncome(c, *userId)
	c.JSON(http.StatusOK, Mappers.IncomeEntityListToDto(*IncomeEntity))
}

func ListIncomeByTime(c *gin.Context) {
	// Done
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))

	db := DB.GormDb
	var Income []Entity.IncomeEntity
	var DateRange DTO.DateRangeDto
	err := c.ShouldBindJSON(&DateRange)
	if err != nil {
		c.JSON(http.StatusBadRequest, "")
		return
	}
	findErr := db.Where("user_with_access_income = ?", userId).Where("date BETWEEN ? AND ?", DateRange.StartDateTime, DateRange.EndDateTime).Find(&Income).Error
	if findErr != nil {
		c.JSON(http.StatusBadRequest, "")
		return
	}
	IncomeDto := Mappers.IncomeEntityListToDto(Income)
	c.JSON(http.StatusOK, IncomeDto)
}

func ListIncomeByTimeAndCategory(c *gin.Context) {
	// Done
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))
	db := DB.GormDb
	id := c.Param("id")
	var Income []Entity.IncomeEntity
	var DateRange DTO.DateRangeDto
	err := c.ShouldBindJSON(&DateRange)
	if err != nil {
		c.JSON(http.StatusBadRequest, "")
		return
	}
	findErr := db.Where("user_with_access_income = ?", userId).Where("date BETWEEN ? AND ?", DateRange.StartDateTime, DateRange.EndDateTime).Where("category_id = ?", id).Find(&Income).Error
	if findErr != nil {
		c.JSON(http.StatusBadRequest, "")
		return
	}
	IncomeDto := Mappers.IncomeEntityListToDto(Income)
	c.JSON(http.StatusOK, IncomeDto)
}

func ReadIncome(c *gin.Context) {
	// Done
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))
	db := DB.GormDb
	var Income Entity.IncomeEntity
	err := db.Where("user_with_access_income = ?", userId).First(&Income, c.Param("id")).Error
	if err != nil {
		c.JSON(http.StatusNotFound, "Record Does Not Exist")
		return
	}
	IncomeDTO := Mappers.IncomeEntityToDto(Income)
	c.JSON(http.StatusOK, IncomeDTO)
}

func CreateIncome(c *gin.Context) {
	// Done
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))

	var IncomeDTO DTO.IncomeDto
	err := c.ShouldBindJSON(&IncomeDTO)
	if err != nil {
		c.JSON(http.StatusBadRequest, "")
		return
	}
	if IncomeDTO.UserId == 0 {
		IncomeDTO.UserId = *userId
	}
	response := Services.CreateIncome(c, *Mappers.IncomeDtoToEntity(IncomeDTO))
	c.JSON(http.StatusOK, Mappers.IncomeEntityToDto(*response))
}

func UpdateIncome(c *gin.Context) {
	// Done
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))
	db := DB.GormDb
	var IncomeDTO DTO.IncomeDto
	err := c.ShouldBindJSON(&IncomeDTO)
	if err != nil {
		c.JSON(http.StatusBadRequest, "")
		panic(err)
	}
	IncomeEntity := *Mappers.IncomeDtoToEntity(IncomeDTO)
	firstErr := db.Where("user_with_access_income = ?", userId).First(&Entity.IncomeEntity{}, IncomeDTO.ID).Error
	if firstErr != nil {
		c.JSON(http.StatusNotFound, "Record Does Not Exist")
		return
	}
	saveErr := db.Save(&IncomeEntity).Error
	if saveErr != nil {
		c.JSON(http.StatusBadRequest, "")
		return
	}
	var UpdatedEntity Entity.IncomeEntity
	updatedErr := db.First(&UpdatedEntity, IncomeDTO.ID).Error
	if updatedErr != nil {
		c.JSON(http.StatusBadRequest, IncomeEntity)
		return
	}
	c.JSON(http.StatusOK, Mappers.IncomeEntityToDto(UpdatedEntity))
}

func DeleteIncome(c *gin.Context) {
	// Done
	userId := UserHelpers.GetUserId(c, UserHelpers.ValidateToken(c))
	id := c.Param("id")
	db := DB.GormDb
	deleteErr := db.Debug().Where("user_with_access_income = ?", userId).Delete(&Entity.IncomeEntity{}, id).Error
	if deleteErr != nil {
		c.JSON(http.StatusNotFound, "Record Not Found")
		return
	}
	c.JSON(http.StatusOK, "")
}
