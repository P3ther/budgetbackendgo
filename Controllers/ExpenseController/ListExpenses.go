package ExpenseController

import (
	"budgetBackendGo/Helpers/UserHelpers"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ListExpenses(c *gin.Context) {
	UserHelpers.ValidateToken(c)
	c.JSON(http.StatusOK, "")
}
