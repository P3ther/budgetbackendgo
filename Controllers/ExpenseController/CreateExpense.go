package ExpenseController

import (
	"budgetBackendGo/Helpers/UserHelpers"
	"budgetBackendGo/Types/DTO"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateExpense(c *gin.Context) {
	UserHelpers.ValidateToken(c)
	var expenseDto DTO.ExpenseDto
	err := c.ShouldBindJSON(&expenseDto)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid Request")
		return
	}
	c.JSON(http.StatusOK, "")
}
