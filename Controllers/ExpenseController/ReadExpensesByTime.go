package ExpenseController

import (
	"budgetBackendGo/Helpers/UserHelpers"
	"budgetBackendGo/Types/DTO"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ReadExpensesByTime(c *gin.Context) {
	UserHelpers.ValidateToken(c)
	var dateRange DTO.DateRangeDto
	err := c.ShouldBindJSON(&dateRange)
	if err != nil {
		c.JSON(http.StatusBadRequest, "Invalid Request")

	}
	c.JSON(http.StatusOK, "")
}
