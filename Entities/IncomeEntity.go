package Entity

import "database/sql"

func (IncomeEntity) TableName() string {
	return "income_entity"
}

type IncomeEntity struct {
	ID                 sql.NullInt64
	Name               sql.NullString
	Date               sql.NullTime
	Price              sql.NullFloat64
	AddressToReceipt   sql.NullString
	Category_id        sql.NullInt64
	Income_currency_id sql.NullInt64
	Investment_id      sql.NullInt64
	UserId             sql.NullInt64 `gorm:"column:user_with_access_income"`
}
