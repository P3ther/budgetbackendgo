package Entity

import (
	"database/sql"
	"time"
)

func (CurrencyPairsEntity) TableName() string {
	return "currency_pairs_entity"
}

type CurrencyPairsEntity struct {
	Id             sql.NullInt64
	FirstCurrency  CurrencyEntity
	SecondCurrency CurrencyEntity
	ExchangeRate   sql.NullFloat64
	LastChange     time.Time
}
