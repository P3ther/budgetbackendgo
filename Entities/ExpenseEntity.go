package Entity

import "database/sql"

func (ExpenseEntity) TableName() string {
	return "expense_entity"
}

type ExpenseEntity struct {
	Id                  sql.NullInt64
	Name                sql.NullString
	Date                sql.NullTime
	Price               sql.NullFloat64
	AddressToReceipt    sql.NullString
	Category_id         sql.NullInt64
	Expense_currency_id sql.NullInt64
	Investment_id       sql.NullInt64
	UserId              sql.NullInt64 `gorm:"column:user_with_access_expense"`
}
type ExpenseEntityResponse struct {
	HttpStatus int
	Expense    ExpenseEntity
}
