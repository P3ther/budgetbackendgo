package Entity

type CurrencyEntity struct {
	ID           int64  `db:"id"`
	Name         string `db:"name"`
	CurrencyCode string `db:"currency_code"`
	Sign         string `db:"sign"`
}
