package Entity

import "database/sql"

func (UserEntity) TableName() string {
	return "user_entity"
}

type UserEntity struct {
	ID               sql.NullInt64
	Password         sql.NullString
	RegistrationCode sql.NullInt64
	UserRole         sql.NullInt64
	Username         sql.NullString
	DefaultCurrency  sql.NullInt64
	DefaultTheme     sql.NullInt64
}
