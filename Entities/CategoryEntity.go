package Entity

import "database/sql"

type Tabler interface {
	TableName() string
}

func (CategoryEntity) TableName() string {
	return "category_entity"
}

type CategoryEntity struct {
	Id                        sql.NullInt64
	Is_visible                sql.NullBool
	Limiting_value            sql.NullFloat64
	Name                      sql.NullString
	Type                      sql.NullInt64
	User_with_access_category sql.NullInt64
	Category_currency         sql.NullInt64
}
