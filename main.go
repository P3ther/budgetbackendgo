package main

import (
	"budgetBackendGo/Controllers"
	"budgetBackendGo/Controllers/ExpenseController"
	DB "budgetBackendGo/Database"
	"budgetBackendGo/Helpers/CommandLineHelpers"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func main() {
	CommandLineHelpers.ParseCommandLine()
	DB.ConnectDb()
	router := gin.Default()
	router.POST("/budget/", Controllers.GetTimedBudget)

	// Category Endpoints
	router.GET("/category/", Controllers.ListCategory)
	router.GET("/category/:id", Controllers.ReadCategory)
	router.POST("/category/", Controllers.CreateCategory)
	router.PUT("/category/", Controllers.UpdateCategory)
	router.DELETE("/category/:id", Controllers.DeleteCategory)

	/* Currencies Endpoints */
	router.GET("/currency/") // Needs Controller
	router.GET("/currencies/", Controllers.ListCurrencies)
	router.POST("/currency/", Controllers.CreateCurrency)
	router.PUT("/currency/", Controllers.UpdateCurrency)
	router.DELETE("/currency/", Controllers.DeleteCurrency)
	/* Expenses Endpoints */
	router.GET("/expenses/", ExpenseController.ListExpenses)
	router.POST("/expenses/time/", ExpenseController.ReadExpensesByTime)
	router.POST("/expense/", ExpenseController.CreateExpense)
	router.DELETE("/expense/", ExpenseController.DeleteExpense)

	// Income Endpoints
	router.GET("/income/", Controllers.ListIncome)
	router.GET("/income/:id", Controllers.ReadIncome)
	router.POST("/income/time/", Controllers.ListIncomeByTime)
	router.POST("/income/time/category/:id", Controllers.ListIncomeByTimeAndCategory)
	router.POST("/income/", Controllers.CreateIncome)
	router.PUT("/income/", Controllers.UpdateIncome)
	router.DELETE("/income/:id", Controllers.DeleteIncome)

	json_data, err := json.Marshal(map[string]string{"username": "Petser", "password": "Test1234!"})
	if err != nil {
		panic(err)
	}
	resp, err := http.Post("https://budgebackendpether.herokuapp.com/login", "application/json",
		bytes.NewBuffer(json_data))
	if err != nil {
		panic(err)
	}
	if resp.StatusCode == 200 {
		fmt.Println("Connection successfull")
	} else if resp.StatusCode == 403 {
		fmt.Println("Wrong Credentials")
	}
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "8080"
	}
	router.Run(":" + port)

}
