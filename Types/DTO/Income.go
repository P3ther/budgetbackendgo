package DTO

import (
	"time"
)

type IncomeDto struct {
	ID                 int64     `json:"id"`
	Name               string    `json:"name"`
	Date               time.Time `json:"date"`
	Price              float64   `json:"price"`
	AddressToReceipt   string    `json:"addressToReceipt"`
	Category_id        int64     `json:"category_id"`
	Income_currency_id int64     `json:"income_currency_id"`
	Investment_id      int64     `json:"investment_id"`
	UserId             int64     `json:"user_id"`
}
