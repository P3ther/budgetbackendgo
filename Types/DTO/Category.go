package DTO

type CategoryDto struct {
	ID                        int64   `json:"id"`
	Is_visible                bool    `json:"is_visible"`
	Limiting_value            float64 `json:"limiting_value"`
	Name                      string  `json:"name"`
	Type                      int64   `json:"type"`
	User_with_access_category int64   `json:"user_with_access_category"`
	Category_currency         int64   `json:"category_currency"`
}
