package DTO

import "time"

type ExpenseDto struct {
	ID               int64     `json:"id"`
	Name             string    `json:"name"`
	Date             time.Time `json:"date"`
	Price            float64   `json:"price"`
	AddressToReceipt string    `json:"addressToReceipt"`
	InvestmentId     int64     `json:"investmentId"`
	CategoryId       int64     `json:"category"`
	CategoryName     string    `json:"categoryName"`
	CurrencyId       int64     `json:"currencyId"`
	CurrencySign     string    `json:"currencySign"`
	UserId           int64     `json:"user_id"`
}

type ExpenseDtoResponse struct {
	HttpStatus int
	Expense    ExpenseDto
}
