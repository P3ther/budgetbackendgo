package DTO

type CurrencyDto struct {
	ID            int32  `json:"id"`
	Name          string `json:"name"`
	Currency_Code string `json:"currencyCode"`
	Sign          string `json:"sign"`
}
