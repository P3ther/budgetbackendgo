package DTO

type BudgetDto struct {
	ID                    int32        `json:"id"`
	ExpensesList          []ExpenseDto `json:"expensesList"`
	IncomeList            []IncomeDto  `json:"incomeList"`
	TotalBalance          float64      `json:"totalBalance"`
	TotalExpense          float64      `json:"totalExpense"`
	TotalIncome           float64      `json:"totalIncome"`
	ExpectedIncome        float64      `json:"expectedIncome"`
	ExpectedExpense       float64      `json:"expectedExpense"`
	TotalInvestmentsValue float64      `json:"totalInvestmentsValue"`
	TotalInvestmentsGain  float64      `json:"totalInvestmentsGain"`
}
