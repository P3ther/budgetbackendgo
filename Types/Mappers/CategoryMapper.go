package Mappers

import (
	Entity "budgetBackendGo/Entities"
	DTO "budgetBackendGo/Types/DTO"
)

func CategoryEntityToDto(entity Entity.CategoryEntity) *DTO.CategoryDto {
	var dto DTO.CategoryDto = DTO.CategoryDto{}
	dto.ID = entity.Id.Int64
	dto.Is_visible = entity.Is_visible.Bool
	dto.Limiting_value = entity.Limiting_value.Float64
	dto.Name = entity.Name.String
	dto.Type = entity.Type.Int64
	dto.User_with_access_category = entity.User_with_access_category.Int64
	dto.Category_currency = entity.Category_currency.Int64
	return &dto
}

func CategoryEntityListToDto(entities []Entity.CategoryEntity) *[]DTO.CategoryDto {
	var dtos []DTO.CategoryDto = []DTO.CategoryDto{}
	for _, entity := range entities {
		result := CategoryEntityToDto(entity)
		dtos = append(dtos, *result)
	}
	return &dtos
}
