package Mappers

import (
	Entity "budgetBackendGo/Entities"
	DTO "budgetBackendGo/Types/DTO"
	"database/sql"
)

func IncomeEntityToDto(entity Entity.IncomeEntity) *DTO.IncomeDto {
	var dto DTO.IncomeDto = DTO.IncomeDto{}
	dto.ID = entity.ID.Int64
	dto.Name = entity.Name.String
	dto.Date = entity.Date.Time
	dto.Price = entity.Price.Float64
	dto.AddressToReceipt = entity.AddressToReceipt.String
	dto.Category_id = entity.Category_id.Int64
	dto.Income_currency_id = entity.Income_currency_id.Int64
	dto.Investment_id = entity.Investment_id.Int64
	dto.UserId = entity.UserId.Int64
	return &dto
}

func IncomeDtoToEntity(dto DTO.IncomeDto) *Entity.IncomeEntity {
	var entity Entity.IncomeEntity = Entity.IncomeEntity{}
	if dto.ID == 0 {
		entity.ID = sql.NullInt64{Int64: dto.ID, Valid: false}
	} else {
		entity.ID = sql.NullInt64{Int64: dto.ID, Valid: true}
	}
	entity.Name = sql.NullString{String: dto.Name, Valid: true}
	entity.Date = sql.NullTime{Time: dto.Date, Valid: true}
	entity.Price = sql.NullFloat64{Float64: dto.Price, Valid: true}
	entity.AddressToReceipt = sql.NullString{String: dto.AddressToReceipt, Valid: true}
	entity.Category_id = sql.NullInt64{Int64: dto.Category_id, Valid: true}
	entity.Income_currency_id = sql.NullInt64{Int64: dto.Income_currency_id, Valid: true}
	if dto.Investment_id == 0 {
		entity.Investment_id = sql.NullInt64{Int64: dto.Investment_id, Valid: false}
	} else {
		entity.Investment_id = sql.NullInt64{Int64: dto.Investment_id, Valid: true}
	}
	entity.UserId = sql.NullInt64{Int64: dto.UserId, Valid: true}
	return &entity
}

func IncomeEntityListToDto(entities []Entity.IncomeEntity) *[]DTO.IncomeDto {
	var dtos []DTO.IncomeDto = []DTO.IncomeDto{}
	for _, entity := range entities {
		result := IncomeEntityToDto(entity)
		dtos = append(dtos, *result)
	}
	return &dtos
}

func IncomeDtoListToEntity(dtos []DTO.IncomeDto) *[]Entity.IncomeEntity {
	var entities []Entity.IncomeEntity = []Entity.IncomeEntity{}
	for _, dto := range dtos {
		result := IncomeDtoToEntity(dto)
		entities = append(entities, *result)
	}
	return &entities
}
