package Mappers

import (
	Entity "budgetBackendGo/Entities"
	DTO "budgetBackendGo/Types/DTO"
	"database/sql"
)

func ExpenseMapperToEntity(dto DTO.ExpenseDto) Entity.ExpenseEntity {
	var entity Entity.ExpenseEntity = Entity.ExpenseEntity{}
	if dto.ID == 0 {
		entity.Id = sql.NullInt64{Int64: dto.ID, Valid: false}
	} else {
		entity.Id = sql.NullInt64{Int64: dto.ID, Valid: true}
	}
	entity.Name = sql.NullString{String: dto.Name, Valid: true}
	entity.Date = sql.NullTime{Time: dto.Date, Valid: true}
	entity.Price = sql.NullFloat64{Float64: dto.Price, Valid: true}
	entity.AddressToReceipt = sql.NullString{String: dto.AddressToReceipt, Valid: true}
	entity.Category_id = sql.NullInt64{Int64: dto.CategoryId, Valid: true}
	entity.Expense_currency_id = sql.NullInt64{Int64: dto.CurrencyId, Valid: true}
	if dto.InvestmentId == 0 {
		entity.Investment_id = sql.NullInt64{Int64: dto.InvestmentId, Valid: false}
	} else {
		entity.Investment_id = sql.NullInt64{Int64: dto.InvestmentId, Valid: true}
	}
	entity.UserId = sql.NullInt64{Int64: dto.UserId, Valid: true}
	return entity
}

func ExpenseMapperToDTO(entity Entity.ExpenseEntity) DTO.ExpenseDto {
	var dto DTO.ExpenseDto = DTO.ExpenseDto{}
	dto.ID = entity.Id.Int64
	dto.Name = entity.Name.String
	dto.Date = entity.Date.Time
	dto.Price = entity.Price.Float64
	dto.AddressToReceipt = entity.AddressToReceipt.String
	dto.CategoryId = entity.Category_id.Int64
	dto.CurrencyId = entity.Expense_currency_id.Int64
	dto.InvestmentId = entity.Investment_id.Int64
	dto.UserId = entity.UserId.Int64
	return dto
}
func ExpenseResponseMapperToDTO(entity Entity.ExpenseEntityResponse) DTO.ExpenseDtoResponse {
	var expenseResponseDto DTO.ExpenseDtoResponse = DTO.ExpenseDtoResponse{}
	expenseResponseDto.HttpStatus = entity.HttpStatus
	expenseResponseDto.Expense = ExpenseMapperToDTO(entity.Expense)
	return expenseResponseDto
}

func ExpensesMapperToDTO(entities []Entity.ExpenseEntity) []DTO.ExpenseDto {
	var response []DTO.ExpenseDto
	for _, element := range entities {
		response = append(response, ExpenseMapperToDTO(element))
	}
	return response
}
