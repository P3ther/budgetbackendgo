package Services

import (
	DB "budgetBackendGo/Database"
	Entity "budgetBackendGo/Entities"
	"budgetBackendGo/Helpers"
	"budgetBackendGo/Types/DTO"
	"budgetBackendGo/Types/Mappers"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetTimedBudget(c *gin.Context, dateRange DTO.DateRangeDto, userId int64) DTO.BudgetDto {
	db := DB.GormDb
	var user Entity.UserEntity
	err := db.First(&user, userId).Error
	if err != nil {
		panic(err)
	}
	fmt.Println(user)
	var Budget DTO.BudgetDto
	var Income []Entity.IncomeEntity
	incomeErr := db.
		Where("date BETWEEN ? AND ?", dateRange.StartDateTime, dateRange.EndDateTime).
		Where("investment_id IS NULL").
		Where("user_with_access_income = ?", userId).
		Order("date desc").
		Find(&Income).
		Error
	if incomeErr != nil {
		c.JSON(http.StatusBadRequest, "")
		panic(incomeErr)
	}
	Budget.IncomeList = *Mappers.IncomeEntityListToDto(Income)
	var Expense []Entity.ExpenseEntity
	expenseErr := db.
		Where("date BETWEEN ? AND ?", dateRange.StartDateTime, dateRange.EndDateTime).
		Where("investment_id IS NULL").
		Where("user_with_access_expense = ?", userId).
		Order("date desc").
		Find(&Expense).
		Error
	if expenseErr != nil {
		c.JSON(http.StatusBadRequest, "")
		panic(expenseErr)
	}
	Budget.ExpensesList = Mappers.ExpensesMapperToDTO(Expense)
	for _, Expense := range Budget.ExpensesList {
		if Expense.CurrencyId != user.DefaultCurrency.Int64 {
			exchangeRate := Helpers.GetExchangeRateToDefaultCurrency(Expense.CurrencyId, user.DefaultCurrency.Int64)
			Budget.TotalBalance -= Expense.Price * exchangeRate
			Budget.TotalExpense += Expense.Price * exchangeRate
		} else {
			Budget.TotalBalance -= Expense.Price
			Budget.TotalExpense += Expense.Price
		}
	}
	for _, Income := range Budget.IncomeList {
		if Income.Income_currency_id != user.DefaultCurrency.Int64 {
			exchangeRate := Helpers.GetExchangeRateToDefaultCurrency(Income.Income_currency_id, user.DefaultCurrency.Int64)
			Budget.TotalBalance += Income.Price * exchangeRate
			Budget.TotalIncome += Income.Price * exchangeRate
		} else {
			Budget.TotalBalance += Income.Price
			Budget.TotalIncome += Income.Price
		}
	}
	expenseCategories := ListCategoryExpense(c, userId)
	incomeCategories := ListCategoryIncome(c, userId)
	for _, expenseCategory := range expenseCategories {
		if expenseCategory.Category_currency.Int64 != user.DefaultCurrency.Int64 {
			exchangeRate := Helpers.GetExchangeRateToDefaultCurrency(expenseCategory.Category_currency.Int64, user.DefaultCurrency.Int64)
			Budget.ExpectedExpense += expenseCategory.Limiting_value.Float64 * exchangeRate
		} else {
			Budget.ExpectedExpense += expenseCategory.Limiting_value.Float64
		}
	}
	for _, incomeCategory := range incomeCategories {
		if incomeCategory.Category_currency.Int64 != user.DefaultCurrency.Int64 {
			exchangeRate := Helpers.GetExchangeRateToDefaultCurrency(incomeCategory.Category_currency.Int64, user.DefaultCurrency.Int64)
			Budget.ExpectedIncome += incomeCategory.Limiting_value.Float64 * exchangeRate
		} else {
			Budget.ExpectedIncome += incomeCategory.Limiting_value.Float64
		}
	}
	return Budget
}
