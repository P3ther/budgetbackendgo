package UserServices

import (
	DB "budgetBackendGo/Database"
	SqlMappers "budgetBackendGo/Database/Mappers"
	SqlRequests "budgetBackendGo/Database/SQL"
	Entity "budgetBackendGo/Entities"
	"log"

	"github.com/gin-gonic/gin"
)

func FindUserByUsername(c *gin.Context, username string) Entity.UserEntity {
	db := DB.DbConnection
	response, err := db.Query(SqlRequests.FindUserByUsername(username))
	if err != nil {
		log.Default().Fatalln(err)
		panic(err)
	}
	return SqlMappers.UserRowMapper(response)
}
