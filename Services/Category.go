package Services

import (
	DB "budgetBackendGo/Database"
	Entity "budgetBackendGo/Entities"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ListCategoryExpense(c *gin.Context, userId int64) []Entity.CategoryEntity {
	db := DB.GormDb
	var Category []Entity.CategoryEntity
	err := db.Debug().Where("type = 0").Where("user_with_access_category = ?", userId).Find(&Category).Error
	if err != nil {
		c.JSON(http.StatusNotFound, "Record Does Not Exist")
		panic(err)
	}
	return Category
}

func ListCategoryIncome(c *gin.Context, userId int64) []Entity.CategoryEntity {
	db := DB.GormDb
	var Category []Entity.CategoryEntity
	err := db.Debug().Where("type = 1").Where("user_with_access_category = ?", userId).Find(&Category).Error
	if err != nil {
		c.JSON(http.StatusNotFound, "Record Does Not Exist")
		panic(err)
	}
	return Category
}
