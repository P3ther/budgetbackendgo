package Services

import (
	DB "budgetBackendGo/Database"
	Entity "budgetBackendGo/Entities"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ListIncome(c *gin.Context, userId int64) *[]Entity.IncomeEntity {
	db := DB.GormDb
	var Income []Entity.IncomeEntity
	err := db.Where("user_with_access_income = ?", userId).Find(&Income).Error
	if err != nil {
		c.JSON(http.StatusNotFound, "Record Not Found")
		panic(err)
	}
	return &Income
}

func CreateIncome(c *gin.Context, IncomeEntity Entity.IncomeEntity) *Entity.IncomeEntity {
	db := DB.GormDb
	createErr := db.Create(&IncomeEntity).Error
	if createErr != nil {
		c.JSON(http.StatusBadRequest, "")
		panic(createErr)
	}
	var CreatedIncome Entity.IncomeEntity
	firstErr := db.First(&CreatedIncome, IncomeEntity.ID).Error
	if firstErr != nil {
		c.JSON(http.StatusBadRequest, "")
		panic(firstErr)
	}
	return &CreatedIncome
}
