package Helpers

import (
	DB "budgetBackendGo/Database"
	SqlMappers "budgetBackendGo/Database/Mappers"
)

func GetExchangeRateToDefaultCurrency(fromCurrencyId int64, defaultCurrencyId int64) float64 {
	db := DB.DbConnection
	response, err := db.Query(`SELECT cpe.id, cpe.exchange_rate, cpe.last_change, ce.id, ce.currency_code, ce."name", ce.sign, ce2.id, ce2.currency_code, ce2."name", ce2.sign  
			FROM currency_pairs_entity cpe 
			Left JOIN currency_entity ce 
			ON cpe.first_currency = ce.id
			Left JOIN currency_entity ce2
			ON cpe.second_currency = ce2.id`)
	if err != nil {
		panic(err)
	}
	CurrencyPairs := SqlMappers.CurrencyPairsRowsMapper(response)
	if err != nil {
		panic(err)
	}
	var exchangeRate float64
	for _, CurrencyPair := range CurrencyPairs {
		if CurrencyPair.FirstCurrency.ID == defaultCurrencyId && CurrencyPair.SecondCurrency.ID == fromCurrencyId {
			var integer float64 = 1
			exchangeRate = integer / CurrencyPair.ExchangeRate.Float64
		} else if CurrencyPair.SecondCurrency.ID == defaultCurrencyId && CurrencyPair.FirstCurrency.ID == fromCurrencyId {
			exchangeRate = CurrencyPair.ExchangeRate.Float64
		}
	}
	return exchangeRate
}
