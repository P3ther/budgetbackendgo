package Helpers

func HandleNullValue(inputValue int32) string {
	if inputValue == 0 {
		return "NULL"
	} else {
		return string(inputValue)
	}
}
