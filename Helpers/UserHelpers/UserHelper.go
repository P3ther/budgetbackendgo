package UserHelpers

import (
	DB "budgetBackendGo/Database"
	Entity "budgetBackendGo/Entities"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func GetToken(c *gin.Context) string {
	return c.Request.Header["Authorization"][0]
}

func ValidateToken(c *gin.Context) string {
	var token string
	if values := c.Request.Header["Authorization"]; len(values) > 0 {
		token = values[0]
	} else {
		c.JSON(http.StatusForbidden, "Not Authorized")
		panic("Not Authorized")
	}
	decodedClaims := jwt.MapClaims{}
	var tokenTrimed string = strings.Replace(token, "Bearer ", "", -1)
	_, err := jwt.ParseWithClaims(tokenTrimed, decodedClaims, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("TOKEN_SECRET")), nil
	})
	if err != nil {
		panic(err)
	}
	decodedTime, err := strconv.ParseInt(strings.Replace(strings.TrimRight(fmt.Sprint(decodedClaims["exp"]), "e+09"), ".", "", -1), 10, 64)
	if err != nil {
		panic(err)
	}
	if !time.Now().Before(time.Unix(decodedTime, 0)) {
		c.JSON(http.StatusForbidden, "Not Authorized")
		panic("Not Authorized")
	}
	return fmt.Sprint(decodedClaims["sub"])
}

func GetUserId(c *gin.Context, username string) *int64 {
	db := DB.GormDb
	var User Entity.UserEntity
	err := db.Where("username = ?", username).First(&User).Error
	if err != nil {
		c.JSON(http.StatusForbidden, "User Does Not Exists")
		panic(err)
	}
	return &User.ID.Int64
}
