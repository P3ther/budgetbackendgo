package SqlRequests

func GetCurrencyPairs() string {
	return `SELECT cpe.id, cpe.exchange_rate, cpe.last_change, ce.id, ce.currency_code, ce."name", ce.sign, ce2.id, ce2.currency_code, ce2."name", ce2.sign  
			FROM currency_pairs_entity cpe 
			Left JOIN currency_entity ce 
			ON cpe.first_currency = ce.id
			Left JOIN currency_entity ce2
			ON cpe.second_currency  = ce2.id`
}
