package SqlRequests

import (
	"budgetBackendGo/Types/DTO"
	"fmt"
)

func GetCurrencies() string {
	return "SELECT * FROM currency_entity"
}

func CreateCurrency(currency DTO.CurrencyDto) string {
	return fmt.Sprintf(`
		INSERT INTO currency_entity (currency_code, name, sign) 
		VALUES ('%s', '%s', '%s') 
		returning currency_entity.id, currency_entity.name, currency_entity.currency_code, currency_entity.sign ;`,
		currency.Currency_Code, currency.Name, currency.Sign)
}

func UpdateCurrency(currency DTO.CurrencyDto) string {
	return fmt.Sprintf(`
		UPDATE currency_entity
		SET currency_code = '%s', name='%s', sign='%s'
		WHERE id = %d
		returning currency_entity.id, currency_entity.name, currency_entity.currency_code, currency_entity.sign ;`,
		currency.Currency_Code, currency.Name, currency.Sign, currency.ID)
}

func DeleteCurrency(currencyId int32) string {
	return fmt.Sprintf(`DELETE FROM currency_entity WHERE id = %d`, currencyId)
}
