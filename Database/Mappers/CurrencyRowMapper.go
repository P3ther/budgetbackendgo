package SqlMappers

import (
	"budgetBackendGo/Types/DTO"
	"database/sql"
	"log"
)

func CurrencyRowMapper(rows *sql.Rows) DTO.CurrencyDto {
	var currency DTO.CurrencyDto
	for rows.Next() {
		err := rows.Scan(
			&currency.ID,
			&currency.Name,
			&currency.Currency_Code,
			&currency.Sign)
		if err != nil {
			log.Default().Println(err)
			panic(err)
		}
	}
	return currency
}
func CurrencyRowsMapper(rows *sql.Rows) []DTO.CurrencyDto {
	var currencies []DTO.CurrencyDto
	for rows.Next() {
		var currency DTO.CurrencyDto
		err := rows.Scan(
			&currency.ID,
			&currency.Name,
			&currency.Currency_Code,
			&currency.Sign)
		if err != nil {
			log.Default().Println(err)
			panic(err)
		}
		currencies = append(currencies, currency)
	}
	return currencies
}
