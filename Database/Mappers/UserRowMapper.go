package SqlMappers

import (
	Entity "budgetBackendGo/Entities"
	"database/sql"
	"log"
)

func UserRowMapper(rows *sql.Rows) Entity.UserEntity {
	var user Entity.UserEntity
	for rows.Next() {
		err := rows.Scan(
			&user.ID,
			&user.Password,
			&user.RegistrationCode,
			&user.UserRole,
			&user.Username,
			&user.DefaultCurrency,
			&user.DefaultTheme,
		)
		if err != nil {
			log.Default().Println(err)
			panic(err)
		}
	}
	return user
}
func UserRowsMapper(rows *sql.Rows) []Entity.UserEntity {
	var users []Entity.UserEntity
	for rows.Next() {
		var user Entity.UserEntity
		err := rows.Scan(
			&user.ID,
			&user.Password,
			&user.RegistrationCode,
			&user.UserRole,
			&user.Username,
			&user.DefaultCurrency,
			&user.DefaultTheme,
		)
		if err != nil {
			log.Default().Println(err)
			panic(err)
		}
		users = append(users, user)
	}
	return users
}
