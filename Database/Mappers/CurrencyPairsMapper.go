package SqlMappers

import (
	Entity "budgetBackendGo/Entities"
	"database/sql"
	"log"
)

func CurrencyPairsRowsMapper(rows *sql.Rows) []Entity.CurrencyPairsEntity {
	var currencyPairs []Entity.CurrencyPairsEntity
	for rows.Next() {
		var currencyPair Entity.CurrencyPairsEntity
		err := rows.Scan(
			&currencyPair.Id,
			&currencyPair.ExchangeRate,
			&currencyPair.LastChange,
			&currencyPair.FirstCurrency.ID,
			&currencyPair.FirstCurrency.CurrencyCode,
			&currencyPair.FirstCurrency.Name,
			&currencyPair.FirstCurrency.Sign,
			&currencyPair.SecondCurrency.ID,
			&currencyPair.SecondCurrency.CurrencyCode,
			&currencyPair.SecondCurrency.Name,
			&currencyPair.SecondCurrency.Sign)
		if err != nil {
			log.Default().Println(err)
			panic(err)
		}
		currencyPairs = append(currencyPairs, currencyPair)
	}
	return currencyPairs
}
