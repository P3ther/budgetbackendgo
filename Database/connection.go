package DB

import (
	"database/sql"
	"fmt"
	"os"
	"strings"

	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var DbConnection *sql.DB
var GormDb *gorm.DB
var Enviroment string

func ConnectDb() *sql.DB {
	if strings.Compare(Enviroment, "Test") == 0 {
		db, err := sql.Open("sqlite3", "file::memory:?cache=shared")
		if err != nil {
			panic(err)
		}
		fmt.Println("This is just a Test Enviroment")
		gormDB, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
		if err != nil {
			panic(err)
		}
		fmt.Println("Successfully connected!")
		DbConnection = db
		GormDb = gormDB
		return db
	} else {
		dbDetails := make(map[string]string)
		dbDetails["HOST"] = os.Getenv("POSTGRES_HOST")
		dbDetails["USER"] = os.Getenv("POSTGRES_USER")
		dbDetails["PASSWORD"] = os.Getenv("POSTGRES_PASSWORD")
		dbDetails["DB"] = os.Getenv("POSTGRES_DB")
		var connectionString string = fmt.Sprintf("host=%s user=%s password=%s dbname=%s", dbDetails["HOST"], dbDetails["USER"], dbDetails["PASSWORD"], dbDetails["DB"])
		db, err := sql.Open("postgres", connectionString)
		if err != nil {
			panic(err)
		}
		gormDB, err := gorm.Open(postgres.New(postgres.Config{
			Conn: db,
		}), &gorm.Config{})
		if err != nil {
			panic(err)
		}
		err = db.Ping()
		if err != nil {
			panic(err)
		}
		fmt.Println("Successfully connected!")
		DbConnection = db
		GormDb = gormDB
		return db
	}
}
